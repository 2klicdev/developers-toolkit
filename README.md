# Requirements

- NodeJS4
- Bower
- npm

# Knowledge requirements

If you develop around 2KLIC frontend development toolkit, you should have experience with:

- AngularJS v1.4+
- NodeJS4
- Javascript
- Bower
- Git
- Gulp

It is highly recommanded to develop in a Linux environment, such as Mac OSX or Ubuntu.
 
# Installation

Make sure need Gulp installed globally:

```sh
$ npm i -g gulp
```

Install the project:

```sh
$ npm install @2klic/developers-toolkit
```

Install project dependencies:
```sh
$ npm install
$ bower install
```

# Run the project

At the root of the project, run the following command to start the server. The project will open in your browser window:
```sh
$ gulp serve
```

Notice that all the modification you do in the ./src folder will trigger the browser to reload the page.

# Project structure

All the source files are into the ./src folder. The only files you need to edit are into ./src/app/components/widget. The "widget" folder is the folder that contains all the files related to your widget to be published. Any file edited outside of this folder will not be published as part of your widget.

# Best practices

During your development, you can get the latest updates about 2KLIC libraries, by running the following commands at the root of your project: 

```sh
$ bower udpate
```

It is highly recommanded to regularly update your project to get the latest version of 2KLIC frontend libraries.

# Publish widget

Once you are ready with your widget, you can simply run the following commands.

First, cd to your widget folder:

```sh
$ cd src/app/components/widget
```

Initialize your widget as a npm package:

```sh
$ npm init
```

Initialize your widget to the Git repository:

```sh
$ git init
```

Stage all files in your widget folder, commit, and push to Git:

```sh
$ git add .
$ git commit -m "my changes are"
$ npm version minor
$ git push origin master
```

Note that npm version major/minor/patche are used only to increase your widget version in package.json. You can read more about this command to know when to use major, minor or patche parameters.

Initialize your widget as a Bower package: 

```sh
$ bower init
```

You can read more about bower package publishing here: [Bower]

Publish to bower:

```sh
$ bower publish
```

Et voilà! You're done. Your widget is now available as a public library; it can be used into 2KLIC frontend as a public widget maintained by you.

Contact 2KLIC development team to get your widget publicly available!


[AngularJS]: <http://angularjs.org>
[Bower]: <http://bower.io/docs/creating-packages/>
