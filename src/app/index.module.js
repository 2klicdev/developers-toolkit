(function () {
  'use strict';

  angular
    .module('developersToolkit', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngMessages',
      'ngAria',
      'restangular',
      'ui.router',
      'DeveloperModule',
      'angularMoment',
      'pascalprecht.translate',
      '2klic-angular'
    ]);

})();
