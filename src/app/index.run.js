(function() {
  'use strict';

  angular
    .module('developersToolkit')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.info('Ready!');

  }

})();
