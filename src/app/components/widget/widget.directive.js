(function() {
  'use strict';

  angular
    .module('DeveloperModule', [])
    .directive('widget', WidgetDirective);

  /** @ngInject */
  function WidgetDirective() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/widget/widget.html',
      scope: {
          param: '='
      },
      controller: WidgetController
    };

    return directive;

    /** @ngInject */
    function WidgetController($scope, $log) {
      $log.info("Widget controller loaded");

    }
  }

})();
