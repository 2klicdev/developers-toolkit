(function() {
  'use strict';

  angular
    .module('developersToolkit')
    .controller('CardsController', CardsController);

  /** @ngInject */
  function CardsController($scope, cards, extra) {

    console.dir(cards)
    console.dir(extra)

    $scope.devices = cards;
    $scope.extra = extra;

    var myWidget = {
      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZXZpY2VJZCI6IjU2YjkwMTI0YmY2YzEyMDQ2NjBjODlhMSIsImV4cCI6MTQ1NjYzODkyMjE0MiwiaWF0IjoxNDU2MDM0MTIyfQ.jo1-H7lBQ1bd1SNUiqwMq-s-HJTm1wtHuDmCUOjzqf8",
      "capabilities": [{
        "_id": "56b90124bf6c1204660c9d8d",
        "device": "56b90124bf6c1204660c89a1",
        "name": "infrared",
        "value": "idle",
        "timestamp": "2016-02-08T20:57:08.871Z"
      }],
      "_id": "56b90124bf6c1204660c89a1",
      "model": {
        "_id": "retrofit_infrared_sensor",
        "protocolId": "iom",
        "gui": {
          "templateName": "iom",
          "icon": "icon-infrared",
          "style": "h4w1",
          "templateUrl": "/manager/Devices/IOM/views/card.html",
          "view": "app/components/widget/widget.html",
          "css": "grid-item--width2 grid-item--height4"
        },
        "type": "infrared sensor",
        "name": "Retrofit Infrared Sensor",
        "createdTs": "2015-06-16T16:42:59.452Z",
        "parameters": [{"value": "0", "parameter": "boardid"}, {"value": "0", "parameter": "inputid"}],
        "capabilities": [{
          "_id": "558052130215d0010090bf64",
          "type": "enum",
          "capabilityId": "infrared",
          "methods": ["get"],
          "units": [],
          "range": ["idle", "detected"]
        }],
        "requireHub": true
      },
      "isEmulated": true,
      "gateway": "56bf88801bd2bcb2a35bac2a",
      "createdBy": "56b900dfdb8a5c2b4abdf8b8",
      "createdTs": "2016-02-08T20:57:08.958Z",
      "status": "ACTIVE",
      "name": "Presence Detection",
      "location": {
        "_id": "56b900e0db8a5c2b4abdf8bc",
        "name": "HUB Device Location",
        "geo": [-73.749358, 45.599616],
        "createdTs": "2016-02-08T20:56:00.681Z",
        "status": "ACTIVE",
        "type": "N/A"
      },
      "onClick": {"state": "^.devices.device", "params": {"deviceId": "56b90124bf6c1204660c89a1"}}
    };

    $scope.devices.push(myWidget);

  }
})();

