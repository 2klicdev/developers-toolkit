(function() {
  'use strict';

  angular
    .module('developersToolkit')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('cards', {
        url: '/cards',
        templateUrl: 'app/components/cards/cards.html',
        controller: 'CardsController',
        controllerAs: 'cards'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
