/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('developersToolkit')

    .factory('cards', function(){

      var cards =  [{
        "model": {
          "_id": "camera_layout",
          "type": "camera-layout",
          "protocolId": "camera",
          "gui": {
            "icon": "icon-camera_rec",
            "style": "h4w1",
            "templateUrl": "/manager/Devices/Camera/views/card.html",
            "css": "grid-item--width2 grid-item--height4",
            "view": "main/device/lib/2klic_devices/camera/views/layout-mobile-2x2.html"
          },
          "name": "Camera Layout",
          "createdTs": "2015-10-24T19:06:41.165Z",
          "parameters": [],
          "capabilities": [],
          "requireHub": false
        }
      }, {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZXZpY2VJZCI6IjU2YjIyZTM4MzhmZjRjNTA0MmRhMWNiOSIsImV4cCI6MTQ1NjYzODkyMjE0MiwiaWF0IjoxNDU2MDM0MTIyfQ.vDoVTjH7g4YoRuO21eG05lHQ4SPhgYjmEUXbJBjQl10",
        "capabilities": [],
        "_id": "56b22e3838ff4c5042da1cb9",
        "isEmulated": true,
        "location": {
          "_id": "56b900e8db8a5c2b4abdf8ce",
          "name": "rootLocation",
          "geo": [-73.749358, 45.599616],
          "createdTs": "2016-02-08T20:56:08.343Z",
          "status": "ACTIVE",
          "type": "N/A"
        },
        "gateway": "56bf88801bd2bcb2a35bac2a",
        "model": {
          "_id": "videostreamer",
          "type": "videostreamer",
          "gui": {
            "templateName": "VideoStreamer",
            "icon": "icon-memory",
            "style": "h1w1",
            "templateUrl": "/manager/Devices/VideoStreamer/views/card.html",
            "view": "main/device/lib/2klic_devices/VideoStreamer/views/card.html"
          },
          "name": "VideoStreamer",
          "createdTs": "2015-11-01T23:27:41.258Z",
          "parameters": [{"parameter": "url", "value": ""}],
          "capabilities": [],
          "requireHub": false
        },
        "parameters": {"url": {"value": "24.37.119.202"}},
        "createdBy": "56b22dba38ff4c5042da1c77",
        "createdTs": "2016-02-03T16:43:36.829Z",
        "status": "ACTIVE",
        "name": "Videostreamer",
        "onClick": {"state": "^.devices.device", "params": {"deviceId": "56b22e3838ff4c5042da1cb9"}}
      }, {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZXZpY2VJZCI6IjU2YjkwMGU0YmY2YzEyMDNmZDAzZjdkOCIsImV4cCI6MTQ1NjYzODkyMjE0MiwiaWF0IjoxNDU2MDM0MTIyfQ.1QCa217ZvV-i7ceQMcF_9oBCaSEWZ82R2fUnj1XaW70",
        "capabilities": [{
          "_id": "56b900e4bf6c1203fd074c10",
          "device": "56b900e4bf6c1203fd03f7d8",
          "name": "switch",
          "value": "on",
          "unit": "testUnit",
          "timestamp": "2016-02-08T20:56:04.523Z"
        }],
        "_id": "56b900e4bf6c1203fd03f7d8",
        "model": {
          "_id": "lsm_15_wall_mount_switch",
          "manufacturer": "Evolve",
          "protocolId": "zwave",
          "protocolName": "zwave_011352573533",
          "gui": {
            "style": "h1w1",
            "templateUrl": "/manager/Devices/Light-Switch/views/card.html",
            "view": "main/device/lib/2klic_devices/undefined/views/card.html"
          },
          "type": "switch",
          "protocolInfo": {
            "manufacturer": [275, 21079, 13619],
            "capabilityMapping": {"switch": {"command": "binary_switch", "interval": 30, "type": "poll"}},
            "associations": []
          },
          "name": "LSM-15 Wall Mount Switch",
          "createdTs": "2015-07-24T20:52:41.212Z",
          "parameters": [],
          "capabilities": [{
            "type": "enum",
            "capabilityId": "switch",
            "_id": "5633d4d9c341f00100944780",
            "methods": ["get", "put"],
            "units": [],
            "range": ["off", "on"]
          }],
          "requireHub": true
        },
        "isEmulated": true,
        "gateway": "56bf88801bd2bcb2a35bac2a",
        "createdBy": "56b900dfdb8a5c2b4abdf8b8",
        "createdTs": "2016-02-08T20:56:04.650Z",
        "status": "ACTIVE",
        "name": "ZWave Switch",
        "location": {
          "_id": "56b900e0db8a5c2b4abdf8bc",
          "name": "HUB Device Location",
          "geo": [-73.749358, 45.599616],
          "createdTs": "2016-02-08T20:56:00.681Z",
          "status": "ACTIVE",
          "type": "N/A"
        },
        "onClick": {"state": "^.devices.device", "params": {"deviceId": "56b900e4bf6c1203fd03f7d8"}}
      }, {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZXZpY2VJZCI6IjU2YjkwMTBjYmY2YzEyMDQxMDBlZTZlYSIsImV4cCI6MTQ1NjYzODkyMjE0MiwiaWF0IjoxNDU2MDM0MTIyfQ.bvUwyFaeri-T1s4XbfAJPg41bCCr5apPTE5B44fyJv4",
        "capabilities": [{
          "_id": "56b9010dbf6c1204100152c8",
          "device": "56b9010cbf6c1204100ee6ea",
          "name": "infrared",
          "value": "idle",
          "timestamp": "2016-02-08T20:56:46.131Z"
        }],
        "_id": "56b9010cbf6c1204100ee6ea",
        "model": {
          "_id": "retrofit_infrared_sensor",
          "protocolId": "iom",
          "gui": {
            "templateName": "iom",
            "icon": "icon-infrared",
            "style": "h1w1",
            "templateUrl": "/manager/Devices/IOM/views/card.html",
            "view": "main/device/lib/2klic_devices/iom/views/card.html"
          },
          "type": "infrared sensor",
          "name": "Retrofit Infrared Sensor",
          "createdTs": "2015-06-16T16:42:59.452Z",
          "parameters": [{"value": "0", "parameter": "boardid"}, {"value": "0", "parameter": "inputid"}],
          "capabilities": [{
            "_id": "558052130215d0010090bf64",
            "type": "enum",
            "capabilityId": "infrared",
            "methods": ["get"],
            "units": [],
            "range": ["idle", "detected"]
          }],
          "requireHub": true
        },
        "isEmulated": true,
        "gateway": "56bf88801bd2bcb2a35bac2a",
        "createdBy": "56b900dfdb8a5c2b4abdf8b8",
        "createdTs": "2016-02-08T20:56:46.391Z",
        "status": "ACTIVE",
        "name": "Presence Detection",
        "location": {
          "_id": "56b900e0db8a5c2b4abdf8bc",
          "name": "HUB Device Location",
          "geo": [-73.749358, 45.599616],
          "createdTs": "2016-02-08T20:56:00.681Z",
          "status": "ACTIVE",
          "type": "N/A"
        },
        "onClick": {"state": "^.devices.device", "params": {"deviceId": "56b9010cbf6c1204100ee6ea"}}
      }, {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZXZpY2VJZCI6IjU2YjkwMTBjYmY2YzEyMDQxMTBlZTcyYyIsImV4cCI6MTQ1NjYzODkyMjE0MiwiaWF0IjoxNDU2MDM0MTIyfQ.pz61nd-w0rCHsbLjLSvsUpTAj6kZpnc5aMXZ0VODdEs",
        "capabilities": [{
          "_id": "56b9010dbf6c1204110250b1",
          "device": "56b9010cbf6c1204110ee72c",
          "name": "infrared",
          "value": "idle",
          "timestamp": "2016-02-08T20:56:46.211Z"
        }],
        "_id": "56b9010cbf6c1204110ee72c",
        "model": {
          "_id": "retrofit_infrared_sensor",
          "protocolId": "iom",
          "gui": {
            "templateName": "iom",
            "icon": "icon-infrared",
            "style": "h1w1",
            "templateUrl": "/manager/Devices/IOM/views/card.html",
            "view": "main/device/lib/2klic_devices/iom/views/card.html"
          },
          "type": "infrared sensor",
          "name": "Retrofit Infrared Sensor",
          "createdTs": "2015-06-16T16:42:59.452Z",
          "parameters": [{"value": "0", "parameter": "boardid"}, {"value": "0", "parameter": "inputid"}],
          "capabilities": [{
            "_id": "558052130215d0010090bf64",
            "type": "enum",
            "capabilityId": "infrared",
            "methods": ["get"],
            "units": [],
            "range": ["idle", "detected"]
          }],
          "requireHub": true
        },
        "isEmulated": true,
        "gateway": "56bf88801bd2bcb2a35bac2a",
        "createdBy": "56b900dfdb8a5c2b4abdf8b8",
        "createdTs": "2016-02-08T20:56:46.989Z",
        "status": "ACTIVE",
        "name": "Presence Detection",
        "location": {
          "_id": "56b900e0db8a5c2b4abdf8bc",
          "name": "HUB Device Location",
          "geo": [-73.749358, 45.599616],
          "createdTs": "2016-02-08T20:56:00.681Z",
          "status": "ACTIVE",
          "type": "N/A"
        },
        "onClick": {"state": "^.devices.device", "params": {"deviceId": "56b9010cbf6c1204110ee72c"}}
      }, {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZXZpY2VJZCI6IjU2YjkwMTEwYmY2YzEyMDQyODAwY2NmMCIsImV4cCI6MTQ1NjYzODkyMjE0MiwiaWF0IjoxNDU2MDM0MTIyfQ.axi8x2yzTvI1mBVnkIZGlUapd5bgWtumyyA9-fNsySw",
        "capabilities": [{
          "_id": "56b90110bf6c1204280105b0",
          "device": "56b90110bf6c12042800ccf0",
          "name": "infrared",
          "value": "idle",
          "timestamp": "2016-02-08T20:56:48.111Z"
        }],
        "_id": "56b90110bf6c12042800ccf0",
        "model": {
          "_id": "retrofit_infrared_sensor",
          "protocolId": "iom",
          "gui": {
            "templateName": "iom",
            "icon": "icon-infrared",
            "style": "h1w1",
            "templateUrl": "/manager/Devices/IOM/views/card.html",
            "view": "main/device/lib/2klic_devices/iom/views/card.html"
          },
          "type": "infrared sensor",
          "name": "Retrofit Infrared Sensor",
          "createdTs": "2015-06-16T16:42:59.452Z",
          "parameters": [{"value": "0", "parameter": "boardid"}, {"value": "0", "parameter": "inputid"}],
          "capabilities": [{
            "_id": "558052130215d0010090bf64",
            "type": "enum",
            "capabilityId": "infrared",
            "methods": ["get"],
            "units": [],
            "range": ["idle", "detected"]
          }],
          "requireHub": true
        },
        "isEmulated": true,
        "gateway": "56bf88801bd2bcb2a35bac2a",
        "createdBy": "56b900dfdb8a5c2b4abdf8b8",
        "createdTs": "2016-02-08T20:56:48.256Z",
        "status": "ACTIVE",
        "name": "Presence Detection",
        "location": {
          "_id": "56b900e0db8a5c2b4abdf8bc",
          "name": "HUB Device Location",
          "geo": [-73.749358, 45.599616],
          "createdTs": "2016-02-08T20:56:00.681Z",
          "status": "ACTIVE",
          "type": "N/A"
        },
        "onClick": {"state": "^.devices.device", "params": {"deviceId": "56b90110bf6c12042800ccf0"}}
      }, {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZXZpY2VJZCI6IjU2YjkwMTI0YmY2YzEyMDQ2NjBjODlhMSIsImV4cCI6MTQ1NjYzODkyMjE0MiwiaWF0IjoxNDU2MDM0MTIyfQ.jo1-H7lBQ1bd1SNUiqwMq-s-HJTm1wtHuDmCUOjzqf8",
        "capabilities": [{
          "_id": "56b90124bf6c1204660c9d8d",
          "device": "56b90124bf6c1204660c89a1",
          "name": "infrared",
          "value": "idle",
          "timestamp": "2016-02-08T20:57:08.871Z"
        }],
        "_id": "56b90124bf6c1204660c89a1",
        "model": {
          "_id": "retrofit_infrared_sensor",
          "protocolId": "iom",
          "gui": {
            "templateName": "iom",
            "icon": "icon-infrared",
            "style": "h1w1",
            "templateUrl": "/manager/Devices/IOM/views/card.html",
            "view": "main/device/lib/2klic_devices/iom/views/card.html"
          },
          "type": "infrared sensor",
          "name": "Retrofit Infrared Sensor",
          "createdTs": "2015-06-16T16:42:59.452Z",
          "parameters": [{"value": "0", "parameter": "boardid"}, {"value": "0", "parameter": "inputid"}],
          "capabilities": [{
            "_id": "558052130215d0010090bf64",
            "type": "enum",
            "capabilityId": "infrared",
            "methods": ["get"],
            "units": [],
            "range": ["idle", "detected"]
          }],
          "requireHub": true
        },
        "isEmulated": true,
        "gateway": "56bf88801bd2bcb2a35bac2a",
        "createdBy": "56b900dfdb8a5c2b4abdf8b8",
        "createdTs": "2016-02-08T20:57:08.958Z",
        "status": "ACTIVE",
        "name": "Presence Detection",
        "location": {
          "_id": "56b900e0db8a5c2b4abdf8bc",
          "name": "HUB Device Location",
          "geo": [-73.749358, 45.599616],
          "createdTs": "2016-02-08T20:56:00.681Z",
          "status": "ACTIVE",
          "type": "N/A"
        },
        "onClick": {"state": "^.devices.device", "params": {"deviceId": "56b90124bf6c1204660c89a1"}}
      }];

      _.map(cards, function(card){ card.model.gui.view = 'app/components/widget/card-test.html' });

      return cards;

    })

    .factory('extra', function(){
      return {"locations":[{"children":[{"children":[],"_id":"56b900e8db8a5c2b4abdf8d2","name":"root.location.location","geo":[-73.749358,45.599616],"parent":"56b900e8db8a5c2b4abdf8d0","createdTs":"2016-02-08T20:56:08.389Z","status":"ACTIVE","type":"N/A"}],"_id":"56b900e8db8a5c2b4abdf8d0","name":"root.location","geo":[-73.749358,45.599616],"parent":"56b900e8db8a5c2b4abdf8ce","createdTs":"2016-02-08T20:56:08.373Z","status":"ACTIVE","type":"N/A"}],"models":[{"_id":"generic_z_wave_device","protocolId":"zwave","protocolName":"zwave_000000000000","gui":{"icon":"icon-door_contact","style":"h1w1","templateUrl":"/manager/Devices/DoorContact/views/card.html"},"type":"generic z-wave","name":"Generic Z-Wave Device","createdTs":"2015-07-24T20:49:14.278Z","parameters":[],"capabilities":[],"requireHub":true},{"_id":"est_ipa3263_ip_camera","type":"camera","manufacturer":"Enster","gui":{"style":"h4w1","templateUrl":"/manager/Devices/Camera/views/card.html"},"name":"EST-IPA3263 IP Camera","createdTs":"2015-10-26T15:28:50.254Z","parameters":[{"value":"192.168.1.123","parameter":"ipaddress"},{"value":"554","parameter":"port"},{"value":"admin","parameter":"username"},{"value":"admin","parameter":"password"},{"value":[{"rtspUrl":"rtsp://${ipaddress}:${port}/user=${username}&password=${password}&channel=1&stream=1.sdp?","label":"First Stream"}],"parameter":"stream"}],"capabilities":[],"requireHub":false},{"_id":"vrp03_scene_capable_plug_in_lamp_dimming_module","manufacturer":"Leviton","protocolId":"zwave","protocolName":"zwave_001D0202030B","type":"dimmer","protocolInfo":{"manufacturer":[29,514,779],"capabilityMapping":{"switch":{"type":"poll","values":[[0,"off"],[255,"on"],["_","on"]],"interval":30,"command":"basic"},"dimmer":{"command":"multilevel_switch","interval":30,"type":"poll"}},"associations":[]},"name":"VRP03 Scene Capable Plug-In Lamp Dimming Module","createdTs":"2015-07-24T20:50:38.547Z","parameters":[],"capabilities":[{"type":"float","capabilityId":"dimmer","_id":"5633d4d8310c0d01002672af","methods":["get","put"],"units":["%"],"range":[{"value":0,"unit":"%"},{"value":100,"unit":"%"}]},{"type":"enum","capabilityId":"switch","_id":"5633d4d8310c0d01002672ae","methods":["get","put"],"units":[],"range":["off","on"]}],"requireHub":true},{"_id":"ipcamera","protocolId":"camera","type":"camera","gui":{"icon":"icon-camera_rec","style":"h4w1","templateUrl":"/manager/Devices/Camera/views/card.html"},"name":"IpCamera","createdTs":"2015-09-21T21:02:49.885Z","parameters":[{"parameter":"ipaddress","value":"0.0.0.0"},{"parameter":"port","value":"554"},{"parameter":"username","value":""},{"parameter":"password","value":""}],"capabilities":[],"requireHub":false},{"_id":"camera_layout","type":"camera-layout","protocolId":"camera","gui":{"icon":"icon-camera_rec","style":"h4w1","templateUrl":"/manager/Devices/Camera/views/card.html","css":"grid-item--width2 grid-item--height4","view":"main/device/lib/2klic_devices/camera/views/layout-mobile-2x2.html"},"name":"Camera Layout","createdTs":"2015-10-24T19:06:41.165Z","parameters":[],"capabilities":[],"requireHub":false},{"_id":"910trl_zw_15_smt_traditional_smartcode_deadbolt_with_z_wave_technology","manufacturer":"Kwikset","protocolId":"zwave","protocolInfo":{"manufacturer":[144,1,1],"capabilityMapping":[{"values":[["secured","locked"],["unsecured","unlocked"],["_","unlocked"]],"command":"door_lock","type":"association","capability":"lock"},{"specific":18,"values":[["_","locked"]],"command":"notification","type":"association","capability":"lock"},{"specific":19,"values":[["_","unlocked"]],"command":"notification","type":"association","capability":"lock"},{"specific":21,"values":[["_","locked"]],"command":"notification","type":"association","capability":"lock"},{"specific":22,"values":[["_","unlocked"]],"command":"notification","type":"association","capability":"lock"},{"specific":24,"values":[["_","locked"]],"command":"notification","type":"association","capability":"lock"},{"specific":25,"values":[["_","unlocked"]],"command":"notification","type":"association","capability":"lock"},{"command":"battery","interval":86400,"type":"interval","capability":"battery"}],"associations":[1]},"protocolName":"zwave_009000010001","type":"lock","name":"910TRL ZW 15 SMT Traditional Smartcode Deadbolt with Z-Wave Technology","createdTs":"2015-09-05T20:19:04.261Z","parameters":[{}],"capabilities":[{"type":"enum","capabilityId":"lock","_id":"5633d4dce1faf30100373dfd","methods":["get","put"],"units":[],"range":["locked","unlocked"]},{"type":"float","capabilityId":"battery","_id":"5633d4dce1faf30100373dfc","methods":["get"],"units":["%"],"range":[{"value":0,"unit":"%"},{"value":100,"unit":"%"}]}],"requireHub":true},{"_id":"lumca_rgb_light","protocolId":"smartpole","type":"light","gui":{"style":"h2w1","templateUrl":"/manager/Devices/Lumca/rgb/views/card.html"},"name":"LUMCA RGB Light","createdTs":"2016-01-25T20:21:24.055Z","parameters":[],"capabilities":[{"_id":"56a683c4a3e4bee0005eae18","capabilityId":"rgb","type":"rgb","methods":["get","put"],"units":[],"range":[]},{"capabilityId":"light","type":"enum","_id":"56aa3315a8dfd28500c2bdd2","methods":["get","put"],"units":[],"range":["off","on","strobe","pulse"]}],"requireHub":true},{"_id":"retrofit_glass_break_sensor","gui":{"templateName":"iom","icon":"icon-glass_break","style":"h1w1","templateUrl":"/manager/Devices/IOM/views/card.html"},"protocolId":"iom","name":"Retrofit Glass Break Sensor","createdTs":"2015-11-09T16:02:36.319Z","parameters":[{"parameter":"boardid","value":"0"},{"parameter":"inputid","value":"0"}],"capabilities":[{"type":"enum","capabilityId":"glass_break","_id":"5640c39c45b9610100a488f0","methods":["get"],"units":[],"range":["idle","detected"]}],"requireHub":true},{"_id":"lumca_110v_plug","protocolId":"smartpole","type":"110V Plug","gui":{"style":"h1w1","templateUrl":"/manager/Devices/Lumca/110v-plug/views/card.html"},"name":"LUMCA 110V Plug","createdTs":"2016-01-25T21:03:27.164Z","parameters":[],"capabilities":[{"type":"enum","capabilityId":"plug","_id":"56a68d9fb07f56e000e666e9","methods":["get","put"],"units":[],"range":["off","on"]}],"requireHub":true},{"_id":"smart_energy_switch","manufacturer":"Aeon Labs","protocolId":"zwave","protocolName":"zwave_008600030006","type":"switch","protocolInfo":{"manufacturer":[134,3,6],"capabilityMapping":{"power":{"command":"multilevel_sensor","interval":17,"type":"poll"},"switch":{"command":"binary_switch","interval":30,"type":"poll"}},"associations":[]},"name":"Smart Energy Switch","createdTs":"2015-07-24T20:52:41.213Z","parameters":[],"capabilities":[{"type":"enum","capabilityId":"switch","_id":"5633d4d9310c0d01002672b1","methods":["get","put"],"units":[],"range":["off","on"]},{"type":"float","precision":3,"capabilityId":"power","_id":"5633d4d9310c0d01002672b0","methods":["get"],"units":["W"],"range":[{"value":0,"unit":"W"},{"value":2400,"unit":"W"}]}],"requireHub":true},{"_id":"retrofit_smoke_detector","protocolId":"iom","gui":{"templateName":"iom","icon":"icon-smoke_sensor","style":"h1w1","templateUrl":"/manager/Devices/IOM/views/card.html"},"type":"smoke detector","name":"Retrofit Smoke Detector","createdTs":"2015-06-16T16:42:59.454Z","parameters":[{"value":"0","parameter":"boardid"},{"value":"0","parameter":"inputid"}],"capabilities":[{"_id":"558052130215d0010090bf65","type":"enum","capabilityId":"smoke","methods":["get"],"units":[],"range":["idle","detected"]}],"requireHub":true},{"_id":"dsb05_zwus_multisensor","manufacturer":"Aeon Labs LLC","protocolId":"zwave","protocolInfo":{"manufacturer":[134,2,5],"configuration":{"Report Interval":{"size":4,"parameter":111},"_enable_reports":{"size":4,"value":225,"parameter":101},"On-Time":{"size":2,"parameter":3}},"capabilityMapping":{"humidity":{"command":"multilevel_sensor","type":"association","specific":"humidity"},"temperature":{"command":"multilevel_sensor","type":"association","specific":"air_temperature"},"luminance":{"command":"multilevel_sensor","type":"association","specific":"luminance"},"battery":{"command":"battery","type":"association"},"motion":{"type":"association","values":[[0,"idle"],["_","detected"]],"command":"basic"}},"associations":[1]},"protocolName":"zwave_008600020005","type":"multisensor","name":"DSB05-ZWUS MultiSensor","createdTs":"2015-07-28T19:39:38.814Z","parameters":[{"parameter":"On-Time","value":240},{"parameter":"Report Interval","value":720}],"capabilities":[{"type":"enum","capabilityId":"motion","_id":"5633d4dbe1faf30100373df4","methods":["get"],"units":[],"range":["idle","detected"]},{"type":"float","precision":1,"capabilityId":"temperature","_id":"5633d4dbe1faf30100373df3","methods":["get"],"units":["C","F"],"range":[{"value":-20,"unit":"C"},{"value":50,"unit":"C"}]},{"type":"float","capabilityId":"luminance","_id":"5633d4dbe1faf30100373df2","methods":["get"],"units":["lx"],"range":[{"value":0,"unit":"lx"},{"value":1000,"unit":"lx"}]},{"type":"float","capabilityId":"humidity","_id":"5633d4dbe1faf30100373df1","methods":["get"],"units":["%"],"range":[{"value":20,"unit":"%"},{"value":90,"unit":"%"}]},{"type":"float","capabilityId":"battery","_id":"5633d4dbe1faf30100373df0","methods":["get"],"units":["%"],"range":[{"value":0,"unit":"%"},{"value":100,"unit":"%"}]}],"requireHub":true},{"_id":"intercom","protocolId":"intercom","gui":{"style":"h4w1","templateUrl":"/manager/Devices/Intercom/views/card.html"},"type":"intercom","manufacturer":"2Klic","name":"Intercom","createdTs":"2015-09-06T21:09:00.897Z","parameters":[{"parameter":"sip","value":""},{"parameter":"domain","value":""},{"parameter":"authorizationUser","value":""},{"parameter":"password","value":""}],"capabilities":[{"_id":"55ecab6ce6c5560100aa882e","methods":[],"units":[],"range":[]}],"requireHub":false},{"_id":"lumca_dimmable_lamp","protocolId":"smartpole","type":"dimmer","gui":{"style":"h2w1","templateUrl":"/manager/Devices/Lumca/Light-Dimmer/views/card.html"},"name":"LUMCA Dimmable Lamp","createdTs":"2016-01-25T20:21:27.580Z","parameters":[],"capabilities":[{"type":"float","capabilityId":"dimmer","_id":"56a683c7b07f56e000e666dc","methods":["get","put"],"units":["%"],"range":[{"unit":"%","value":0},{"unit":"%","value":100}]}],"requireHub":true},{"_id":"hsm100_multi_sensor","manufacturer":"Express Controls","protocolId":"zwave","protocolName":"zwave_001E00020001","type":"multisensor","protocolInfo":{"manufacturer":[30,2,1],"configuration":{"TempAdj":{"parameter":7},"Light Threshold":{"parameter":4},"Stay Awake":{"parameter":5},"LED On/Off":{"parameter":3},"Sensitivity":{"signed":false,"parameter":1},"On-Time":{"parameter":2},"Wake Up Interval":{"parameter":"wake_up_interval"},"On Value":{"parameter":6}},"capabilityMapping":{"temperature":{"channel":3,"command":"multilevel_sensor","type":"wakeup"},"luminance":{"channel":2,"command":"multilevel_sensor","type":"wakeup"},"battery":{"command":"battery","type":"wakeup"},"motion":{"type":"association","values":[[0,"idle"],["_","detected"]],"command":"basic"}},"associations":[1]},"name":"HSM100 Multi-Sensor","createdTs":"2015-07-24T20:52:41.218Z","parameters":[{"parameter":"Sensitivity","value":200},{"parameter":"On-Time","value":20},{"parameter":"LED On/Off","value":-1},{"parameter":"Light Threshold","value":100},{"parameter":"Stay Awake","value":0},{"parameter":"On Value","value":-1},{"parameter":"TempAdj","value":0},{"parameter":"Wake Up Interval","value":3600}],"capabilities":[{"type":"enum","capabilityId":"motion","_id":"5633d4dac341f00100944786","methods":["get"],"units":[],"range":["idle","detected"]},{"type":"float","precision":1,"capabilityId":"temperature","_id":"5633d4dac341f00100944785","methods":["get"],"units":["F"],"range":[{"value":20,"unit":"F"},{"value":150,"unit":"F"}]},{"type":"float","capabilityId":"luminance","_id":"5633d4dac341f00100944784","methods":["get"],"units":["%"],"range":[{"value":0,"unit":"%"},{"value":100,"unit":"%"}]},{"type":"float","capabilityId":"battery","_id":"5633d4dac341f00100944783","methods":["get"],"units":["%"],"range":[{"value":0,"unit":"%"},{"value":100,"unit":"%"}]}],"requireHub":true},{"_id":"t100r_z_wave_thermostat","manufacturer":"Evolve","protocolId":"zwave","gui":{"style":"h2w1","templateUrl":"/manager/Devices/Thermostat/views/card.html"},"protocolInfo":{"manufacturer":[139,21586,21558],"capabilityMapping":{"fan_mode":{"type":"association","values":[["on_low","on"],["auto_low","auto"],["on_high","high"]],"command":"thermostat_fan_mode"},"cooling":{"command":"thermostat_setpoint","type":"association","specific":"cooling_1"},"mode":{"command":"thermostat_mode","type":"association"},"fan_state":{"command":"thermostat_fan_state","type":"association"},"temperature":{"command":"multilevel_sensor","type":"association"},"heating":{"command":"thermostat_setpoint","type":"association","specific":"heating_1"},"state":{"command":"thermostat_operating_state","type":"association"}},"associations":[1]},"protocolName":"zwave_011354485447","type":"thermostat","name":"T100R Z-Wave Thermostat","createdTs":"2015-07-28T19:39:39.708Z","parameters":[],"capabilities":[{"type":"float","capabilityId":"temperature","_id":"5633d4db310c0d01002672c5","methods":["get"],"units":[],"range":[{"value":-40,"unit":"F"},{"value":190,"unit":"F"}]},{"type":"enum","capabilityId":"mode","_id":"5633d4db310c0d01002672c4","methods":["get","put"],"units":[],"range":["off","heat","cool","auto","aux_heat"]},{"type":"enum","capabilityId":"state","_id":"5633d4db310c0d01002672c3","methods":["get"],"units":[],"range":["idle","heating","cooling","fan_only","pending_heat","pending_cool","eco"]},{"type":"float","capabilityId":"heating","_id":"5633d4db310c0d01002672c2","methods":["get","put"],"units":["C","F"],"range":[{"value":4,"unit":"C"},{"value":42,"unit":"C"}]},{"type":"float","capabilityId":"cooling","_id":"5633d4db310c0d01002672c1","methods":["get","put"],"units":["C","F"],"range":[{"value":6,"unit":"C"},{"value":45,"unit":"C"}]},{"type":"enum","capabilityId":"fan_mode","_id":"5633d4db310c0d01002672c0","methods":["get","put"],"units":[],"range":["auto","on","high"]},{"type":"enum","capabilityId":"fan_state","_id":"5633d4db310c0d01002672bf","methods":["get"],"units":[],"range":["off","running_low","running_high","running_medium","circulation","humidity_circulation","right_left_circulation","up_down_circulation","quiet_circulation"]}],"requireHub":true},{"_id":"videostreamer","type":"videostreamer","gui":{"templateName":"VideoStreamer","icon":"icon-memory","style":"h1w1","templateUrl":"/manager/Devices/VideoStreamer/views/card.html","view":"main/device/lib/2klic_devices/VideoStreamer/views/card.html"},"name":"VideoStreamer","createdTs":"2015-11-01T23:27:41.258Z","parameters":[{"parameter":"url","value":""}],"capabilities":[],"requireHub":false},{"_id":"retrofit_door_contact","protocolId":"iom","gui":{"templateName":"iom","icon":"icon-door_contact","style":"h1w1","templateUrl":"/manager/Devices/IOM/views/card.html"},"type":"door contact","name":"Retrofit Door Contact","createdTs":"2015-06-16T16:42:59.438Z","parameters":[{"value":"0","parameter":"boardid"},{"value":"0","parameter":"inputid"}],"capabilities":[{"_id":"558052130215d0010090bf62","type":"enum","capabilityId":"door","methods":["get"],"units":[],"range":["closed","open"]}],"requireHub":true},{"_id":"tz45_z_wave_thermostat","manufacturer":"RCS","protocolId":"zwave","protocolName":"zwave_008B54525436","type":"thermostat","protocolInfo":{"manufacturer":[139,21586,21558],"capabilityMapping":{"fan_mode":{"type":"association","values":[["on_low","on"],["auto_low","auto"]],"command":"thermostat_fan_mode"},"cooling":{"command":"thermostat_setpoint","type":"association","specific":"cooling_1"},"mode":{"command":"thermostat_mode","type":"association"},"fan_state":{"command":"thermostat_fan_state","type":"association"},"temperature":{"command":"multilevel_sensor","type":"association"},"heating":{"command":"thermostat_setpoint","type":"association","specific":"heating_1"},"state":{"command":"thermostat_operating_state","type":"association"}},"associations":[1]},"gui":{"style":"h2w1","templateUrl":"/manager/Devices/Thermostat/views/card.html"},"name":"TZ45 Z-Wave Thermostat","createdTs":"2015-07-24T20:52:41.221Z","parameters":[],"capabilities":[{"_id":"5633d4da310c0d01002672be","capabilityId":"temperature","type":"float","methods":["get"],"units":[],"range":[{"unit":"F","value":-40},{"unit":"F","value":190}]},{"_id":"5633d4da310c0d01002672bd","capabilityId":"mode","type":"enum","methods":["get","put"],"units":[],"range":["off","heat","cool","auto","aux_heat"]},{"_id":"5633d4da310c0d01002672bc","capabilityId":"state","type":"enum","methods":["get"],"units":[],"range":["idle","heating","cooling","fan_only","pending_heat","pending_cool","eco"]},{"_id":"5633d4da310c0d01002672bb","capabilityId":"heating","type":"float","methods":["get","put"],"units":["C","F"],"range":[{"unit":"C","value":4},{"unit":"C","value":32}]},{"_id":"5633d4da310c0d01002672ba","capabilityId":"cooling","type":"float","methods":["get","put"],"units":["C","F"],"range":[{"unit":"C","value":10},{"unit":"C","value":37}]},{"_id":"5633d4da310c0d01002672b9","capabilityId":"fan_mode","type":"enum","methods":["get","put"],"units":[],"range":["auto","on"]},{"_id":"5633d4da310c0d01002672b8","capabilityId":"fan_state","type":"enum","methods":["get"],"units":[],"range":["off","running_low","running_high","running_medium","circulation","humidity_circulation","right_left_circulation","up_down_circulation","quiet_circulation"]}],"requireHub":true},{"_id":"retrofit_water_sensor","protocolId":"iom","gui":{"templateName":"iom","icon":"icon-water_sensor","style":"h1w1","templateUrl":"/manager/Devices/IOM/views/card.html"},"type":"water sensor","name":"Retrofit Water Sensor","createdTs":"2015-06-16T16:42:59.462Z","parameters":[{"value":"0","parameter":"boardid"},{"value":"0","parameter":"inputid"}],"capabilities":[{"_id":"558052130215d0010090bf66","type":"enum","capabilityId":"water","methods":["get"],"units":[],"range":["idle","detected"]}],"requireHub":true},{"_id":"vrmx1_1l_vizia_rf_scene_capable_incandescentmagnetic_low_voltage_or_fluorescent_dimmer","manufacturer":"Leviton","protocolId":"zwave","gui":{"style":"h2w1","templateUrl":"/manager/Devices/Light-Dimmer/views/card.html"},"protocolInfo":{"manufacturer":[29,1025,521],"capabilityMapping":{"switch":{"type":"hail","values":[[0,"off"],[255,"on"],["_","on"]],"command":"basic"},"dimmer":{"command":"multilevel_switch","type":"hail"}},"associations":[1]},"protocolName":"zwave_001D04010209","type":"dimmer","name":"VRMX1-1L Vizia RF+ Scene Capable Incandescent/Magnetic Low Voltage or Fluorescent Dimmer","createdTs":"2015-07-29T15:28:05.504Z","parameters":[],"capabilities":[{"type":"float","capabilityId":"dimmer","_id":"5633d4d8c341f0010094477f","methods":["get","put"],"units":["%"],"range":[{"value":0,"unit":"%"},{"value":100,"unit":"%"}]},{"type":"enum","capabilityId":"switch","_id":"5633d4d8c341f0010094477e","methods":["get","put"],"units":[],"range":["off","on"]}],"requireHub":true},{"_id":"smart_controller_v1","type":"Smart Controller","manufacturer":"2KLIC inc.","gui":{"style":"h2w1","templateUrl":"/manager/Devices/SmartController/views/card.html"},"name":"Smart Controller v1","createdTs":"2016-02-05T20:23:36.967Z","parameters":[],"capabilities":[],"requireHub":true},{"_id":"retrofit_window_contact","protocolId":"iom","gui":{"templateName":"iom","icon":"icon-window_contact","style":"h1w1","templateUrl":"/manager/Devices/IOM/views/card.html"},"type":"window contact","name":"Retrofit Window Contact","createdTs":"2015-06-16T16:42:59.446Z","parameters":[{"value":"0","parameter":"boardid"},{"value":"0","parameter":"inputid"}],"capabilities":[{"_id":"558052130215d0010090bf63","type":"enum","capabilityId":"window","methods":["get"],"units":[],"range":["closed","open"]}],"requireHub":true},{"_id":"hsm02_0_doorwindow_contact","manufacturer":"Everspring Ind. Co., Ltd.","protocolId":"zwave","protocolName":"zwave_006000020002","type":"contact","protocolInfo":{"manufacturer":[96,2,2],"configuration":{"Wake Up Interval":{"parameter":"wake_up_interval"}},"capabilityMapping":{"contact":{"type":"association","values":[[0,"closed"],["_","open"]],"command":"basic"},"battery":{"command":"battery","type":"wakeup"}},"associations":[2]},"name":"HSM02-0 Door/Window Contact","createdTs":"2015-07-24T20:52:41.216Z","parameters":[{"parameter":"Wake Up Interval","value":86400}],"capabilities":[{"type":"enum","capabilityId":"contact","_id":"5633d4d9c341f00100944782","methods":["get"],"units":[],"range":["closed","open"]},{"type":"float","capabilityId":"battery","_id":"5633d4d9c341f00100944781","methods":["get"],"units":["%"],"range":[{"value":0,"unit":"%"},{"value":100,"unit":"%"}]}],"requireHub":true},{"_id":"retrofit_infrared_sensor","protocolId":"iom","gui":{"templateName":"iom","icon":"icon-infrared","style":"h1w1","templateUrl":"/manager/Devices/IOM/views/card.html","view":"main/device/lib/2klic_devices/iom/views/card.html"},"type":"infrared sensor","name":"Retrofit Infrared Sensor","createdTs":"2015-06-16T16:42:59.452Z","parameters":[{"value":"0","parameter":"boardid"},{"value":"0","parameter":"inputid"}],"capabilities":[{"_id":"558052130215d0010090bf64","type":"enum","capabilityId":"infrared","methods":["get"],"units":[],"range":["idle","detected"]}],"requireHub":true},{"_id":"th8320zw_touch_screen_thermostat","manufacturer":"Honeywell","protocolId":"zwave","protocolName":"zwave_003900110001","type":"thermostat","protocolInfo":{"manufacturer":[57,17,1],"capabilityMapping":{"fan_mode":{"type":"association","values":[["on_low","on"],["auto_low","auto"],["circulation","circulation"]],"command":"thermostat_fan_mode"},"cooling":{"command":"thermostat_setpoint","type":"association","specific":"cooling_1"},"mode":{"command":"thermostat_mode","type":"association"},"fan_state":{"command":"thermostat_fan_state","type":"association"},"temperature":{"command":"multilevel_sensor","type":"association"},"heating":{"command":"thermostat_setpoint","type":"association","specific":"heating_1"},"state":{"command":"thermostat_operating_state","type":"association"}},"associations":[1]},"name":"TH8320ZW Touch-Screen Thermostat","createdTs":"2015-07-24T20:52:41.223Z","parameters":[],"capabilities":[{"type":"float","precision":1,"capabilityId":"temperature","_id":"5633d4dac341f0010094478d","methods":["get"],"units":[],"range":[{"value":-40,"unit":"F"},{"value":190,"unit":"F"}]},{"type":"enum","capabilityId":"mode","_id":"5633d4dac341f0010094478c","methods":["get","put"],"units":[],"range":["off","heat","cool","aux_heat","energy_save_heat","energy_save_cool"]},{"type":"enum","capabilityId":"state","_id":"5633d4dac341f0010094478b","methods":["get"],"units":[],"range":["idle","heating","cooling","pending_heat","pending_cool"]},{"type":"float","precision":1,"capabilityId":"heating","_id":"5633d4dac341f0010094478a","methods":["get","put"],"units":["C","F"],"range":[{"value":4.5,"unit":"C"},{"value":32,"unit":"C"}]},{"type":"float","precision":1,"capabilityId":"cooling","_id":"5633d4dac341f00100944789","methods":["get","put"],"units":["C","F"],"range":[{"value":10,"unit":"C"},{"value":37,"unit":"C"}]},{"type":"enum","capabilityId":"fan_mode","_id":"5633d4dac341f00100944788","methods":["get","put"],"units":[],"range":["auto","on","circulation"]},{"type":"enum","capabilityId":"fan_state","_id":"5633d4dac341f00100944787","methods":["get"],"units":[],"range":["off","running_low","running_high","running_medium","circulation","humidity_circulation","right_left_circulation","up_down_circulation","quiet_circulation"]}],"requireHub":true},{"_id":"evolve_thermostat...","manufacturer":"Evolve","protocolId":"zwave","protocolInfo":{"manufacturer":[275,17750,21556],"capabilityMapping":{"fan_mode":{"type":"association","values":[["on_low","on"],["auto_low","auto"]],"command":"thermostat_fan_mode"},"cooling":{"command":"thermostat_setpoint","type":"association","specific":"cooling_1"},"mode":{"command":"thermostat_mode","type":"association"},"fan_state":{"command":"thermostat_fan_state","type":"association"},"temperature":{"command":"multilevel_sensor","type":"association"},"heating":{"command":"thermostat_setpoint","type":"association","specific":"heating_1"},"state":{"command":"thermostat_operating_state","type":"association"}},"associations":[1]},"protocolName":"zwave_011345565434","type":"thermostat","name":"Evolve Thermostat...","createdTs":"2015-07-31T19:53:35.259Z","parameters":[],"capabilities":[{"type":"float","capabilityId":"temperature","_id":"5633d4dce1faf30100373dfb","methods":["get"],"units":[],"range":[{"value":-40,"unit":"F"},{"value":190,"unit":"F"}]},{"type":"enum","capabilityId":"mode","_id":"5633d4dce1faf30100373dfa","methods":["get","put"],"units":[],"range":["off","heat","cool","auto","aux_heat"]},{"type":"enum","capabilityId":"state","_id":"5633d4dce1faf30100373df9","methods":["get"],"units":[],"range":["idle","heating","cooling","fan_only","pending_heat","pending_cool","eco"]},{"type":"float","capabilityId":"heating","_id":"5633d4dce1faf30100373df8","methods":["get","put"],"units":["C","F"],"range":[{"value":4,"unit":"C"},{"value":32,"unit":"C"}]},{"type":"float","capabilityId":"cooling","_id":"5633d4dce1faf30100373df7","methods":["get","put"],"units":["C","F"],"range":[{"value":10,"unit":"C"},{"value":37,"unit":"C"}]},{"type":"enum","capabilityId":"fan_mode","_id":"5633d4dce1faf30100373df6","methods":["get","put"],"units":[],"range":["auto","on"]},{"type":"enum","capabilityId":"fan_state","_id":"5633d4dce1faf30100373df5","methods":["get"],"units":[],"range":["off","running_low","running_high","running_medium","circulation","humidity_circulation","right_left_circulation","up_down_circulation","quiet_circulation"]}],"requireHub":true},{"_id":"lumca_car_charger","protocolId":"smartpole","type":"car charger","gui":{"style":"h1w1","templateUrl":"/manager/Devices/Lumca/car-charger/views/card.html"},"name":"LUMCA Car Charger","createdTs":"2016-01-25T20:21:24.651Z","parameters":[],"capabilities":[{"type":"enum","capabilityId":"power","_id":"56a683c4faa3d7ea0074269b","methods":["get","put"],"units":[],"range":["off","on"]},{"type":"enum","capabilityId":"charge","_id":"56a683c4faa3d7ea0074269a","methods":["get","put"],"units":[],"range":["off","on"]},{"type":"enum","capabilityId":"fault","_id":"56a683c4faa3d7ea00742699","methods":["get","put"],"units":[],"range":["off","on"]},{"type":"enum","capabilityId":"alert","_id":"56a683c4faa3d7ea00742698","methods":["get","put"],"units":[],"range":["off","on"]}],"requireHub":true},{"_id":"lumca_usb_plug","type":"USB","protocolId":"smartpole","gui":{"style":"h1w1","templateUrl":"/manager/Devices/Lumca/usb-plug/views/card.html"},"name":"LUMCA USB Plug","createdTs":"2016-01-25T21:03:23.435Z","parameters":[],"capabilities":[{"capabilityId":"usb","type":"enum","_id":"56a68d9ba3e4bee0005eae22","methods":["get","put"],"units":[],"range":["off","on"]}],"requireHub":true},{"_id":"lsm_15_wall_mount_switch","manufacturer":"Evolve","protocolId":"zwave","protocolName":"zwave_011352573533","gui":{"style":"h1w1","templateUrl":"/manager/Devices/Light-Switch/views/card.html","view":"main/device/lib/2klic_devices/undefined/views/card.html"},"type":"switch","protocolInfo":{"manufacturer":[275,21079,13619],"capabilityMapping":{"switch":{"command":"binary_switch","interval":30,"type":"poll"}},"associations":[]},"name":"LSM-15 Wall Mount Switch","createdTs":"2015-07-24T20:52:41.212Z","parameters":[],"capabilities":[{"type":"enum","capabilityId":"switch","_id":"5633d4d9c341f00100944780","methods":["get","put"],"units":[],"range":["off","on"]}],"requireHub":true},{"_id":"dwc_mf21m4tir_ip_camera","type":"camera","manufacturer":"Digital Watchdog","gui":{"templateName":"Camera","icon":"icon-camera_rec","style":"h4w1","templateUrl":"/manager/Devices/Camera/views/card.html"},"name":"DWC-MF21M4TIR IP Camera","createdTs":"2015-10-26T13:15:07.013Z","parameters":[{"parameter":"ipaddress","value":"192.168.1.123"},{"parameter":"port","value":"554"},{"parameter":"username","value":"admin"},{"parameter":"password","value":"admin"},{"parameter":"stream","value":[{"label":"First Stream","rtspUrl":"rtsp://${username}:${password}@${ipaddress}:${port}/h264"}]}],"capabilities":[],"requireHub":false},{"_id":"retrofit_panic_button","protocolId":"iom","gui":{"templateName":"iom","icon":"icon-panic_button","style":"h1w1","templateUrl":"/manager/Devices/IOM/views/card.html"},"type":"panic button","name":"Retrofit Panic Button","createdTs":"2015-09-23T20:10:00.268Z","parameters":[{"parameter":"boardid","value":"0"},{"parameter":"inputid","value":"0"}],"capabilities":[{"_id":"56030718a46da901000872ba","capabilityId":"panic","type":"enum","methods":["get"],"units":[],"range":["idle","detected"]}],"requireHub":true},{"_id":"dsb29_zwus_doorwindow_sensor","manufacturer":"Aeon Labs LLC","protocolId":"zwave","protocolName":"zwave_00860002001D","type":"contact","gui":{"icon":"icon-door_contact","style":"h1w1","templateUrl":"/manager/Devices/DoorContact/views/card.html"},"protocolInfo":{"manufacturer":[134,2,29],"configuration":{"Wake Up Interval":{"parameter":"wake_up_interval"}},"capabilityMapping":{"contact":{"type":"association","values":[[0,"closed"],["_","open"]],"command":"basic"},"tamper":{"values":[[0,"ok"],["_","detected"]],"command":"notification","type":"association","specific":0},"battery":{"command":"battery","type":"wakeup"}},"associations":[1]},"name":"DSB29-ZWUS Door/Window Sensor","createdTs":"2015-07-24T20:52:41.214Z","parameters":[{"parameter":"Wake Up Interval","value":0}],"capabilities":[{"type":"enum","capabilityId":"contact","_id":"5633d4dbe1faf30100373def","methods":["get"],"units":[],"range":["closed","open"]},{"type":"enum","capabilityId":"tamper","_id":"5633d4dbe1faf30100373dee","methods":["get"],"units":[],"range":["ok","detected"]},{"type":"float","capabilityId":"battery","_id":"5633d4dbe1faf30100373ded","methods":["get"],"units":["%"],"range":[{"value":0,"unit":"%"},{"value":100,"unit":"%"}]}],"requireHub":true},{"_id":"zw100_a_multisensor_6","manufacturer":"Aeon Labs LLC","protocolId":"zwave","protocolInfo":{"manufacturer":[134,258,100],"configuration":{"_enable_battery":{"size":4,"value":1,"parameter":102},"_enable_motion":{"size":1,"value":1,"parameter":4},"_enable_reports":{"size":4,"value":226,"parameter":101},"On-Time":{"size":2,"parameter":3},"Report Interval":{"size":4,"parameter":111},"_set_battery_interval":{"size":4,"value":86400,"parameter":112}},"capabilityMapping":{"humidity":{"command":"multilevel_sensor","type":"association","specific":"humidity"},"battery":{"command":"battery","type":"association"},"temperature":{"command":"multilevel_sensor","type":"association","specific":"air_temperature"},"ultraviolet":{"command":"multilevel_sensor","type":"association","specific":"ultraviolet"},"luminance":{"command":"multilevel_sensor","type":"association","specific":"luminance"},"motion":{"type":"association","values":[[0,"idle"],["_","detected"]],"command":"basic"}},"associations":[1]},"protocolName":"zwave_008601020064","type":"multisensor","name":"ZW100-A MultiSensor 6","createdTs":"2015-09-05T20:19:00.087Z","parameters":[{"parameter":"On-Time","value":240},{"parameter":"Report Interval","value":720}],"capabilities":[{"type":"enum","capabilityId":"motion","_id":"5633d4d9310c0d01002672b7","methods":["get"],"units":[],"range":["idle","detected"]},{"type":"float","capabilityId":"temperature","_id":"5633d4d9310c0d01002672b6","methods":["get"],"units":["C","F"],"range":[{"value":-10,"unit":"C"},{"value":50,"unit":"C"}]},{"type":"float","capabilityId":"humidity","_id":"5633d4d9310c0d01002672b5","methods":["get"],"units":["%"],"range":[{"value":20,"unit":"%"},{"value":90,"unit":"%"}]},{"type":"float","capabilityId":"luminance","_id":"5633d4d9310c0d01002672b4","methods":["get"],"units":["lx"],"range":[{"value":0,"unit":"lx"},{"value":30000,"unit":"lx"}]},{"type":"float","capabilityId":"ultraviolet","_id":"5633d4d9310c0d01002672b3","methods":["get"],"units":["UV index"],"range":[{"value":0,"unit":"UV index"},{"value":26,"unit":"UV index"}]},{"type":"float","capabilityId":"battery","_id":"5633d4d9310c0d01002672b2","methods":["get"],"units":["%"],"range":[{"value":0,"unit":"%"},{"value":100,"unit":"%"}]}],"requireHub":true},{"_id":"lom_15_duplex_wall_outlet","manufacturer":"Evolve","protocolId":"zwave","protocolName":"zwave_011352523530","type":"plug","protocolInfo":{"manufacturer":[275,21074,13616],"capabilityMapping":{"switch":{"command":"binary_switch","interval":30,"type":"poll"}},"associations":[]},"name":"LOM-15 Duplex Wall Outlet","createdTs":"2015-07-24T20:52:41.210Z","parameters":[],"capabilities":[{"type":"enum","capabilityId":"switch","_id":"5633d4dae1faf30100373dec","methods":["get","put"],"units":[],"range":["off","on"]}],"requireHub":true}],"alarm":{"zones":[{"_id":"56b9010fbf6c1204160260e8","device":"56b90124bf6c1204660c89a1","capability":"infrared","normalState":"idle","sensorStatus":"normal","zoneStatus":"disarmed","alarm":"56b900dbdb8a5c2b4abdf8b6","exitDelay":33,"entryDelay":22,"timestamp":"2016-01-12T21:33:32.992Z"},{"_id":"56b9010fbf6c12041f0873f5","device":"56b9010cbf6c1204110ee72c","capability":"infrared","normalState":"idle","sensorStatus":"normal","zoneStatus":"disarmed","alarm":"56b900dbdb8a5c2b4abdf8b6","timestamp":"2016-01-12T21:33:32.992Z"},{"_id":"56b90127bf6c12048f0d9459","device":"56b90124bf6c1204660c89a1","capability":"infrared","normalState":"idle","sensorStatus":"normal","zoneStatus":"disarmed","entryDelay":77,"exitDelay":33,"alarm":"56b900dbdb8a5c2b4abdf8b6","timestamp":"2016-01-12T21:33:32.992Z"}],"_id":"56b900dbdb8a5c2b4abdf8b6","name":"TEST ALARM","gateway":"56bf88801bd2bcb2a35bac2a","location":"56b900e0db8a5c2b4abdf8bc","status":"disarmed"},"jwPlayerControls":true,"videoStreamer":{"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZXZpY2VJZCI6IjU2YjIyZTM4MzhmZjRjNTA0MmRhMWNiOSIsImV4cCI6MTQ1NjYzOTA3ODYzMiwiaWF0IjoxNDU2MDM0Mjc4fQ.1rwGg4AjZynGJ8cgULMKdeJknAKeDsgn9YKtlU92bcQ","capabilities":[],"_id":"56b22e3838ff4c5042da1cb9","isEmulated":true,"location":{"_id":"56b900e8db8a5c2b4abdf8ce","name":"rootLocation","geo":[-73.749358,45.599616],"createdTs":"2016-02-08T20:56:08.343Z","status":"ACTIVE","type":"N/A"},"gateway":"56bf88801bd2bcb2a35bac2a","model":{"_id":"videostreamer","type":"videostreamer","gui":{"templateName":"VideoStreamer","icon":"icon-memory","style":"h1w1","templateUrl":"/manager/Devices/VideoStreamer/views/card.html","view":"main/device/lib/2klic_devices/VideoStreamer/views/card.html"},"name":"VideoStreamer","createdTs":"2015-11-01T23:27:41.258Z","parameters":[{"parameter":"url","value":""}],"capabilities":[],"requireHub":false},"parameters":{"url":{"value":"24.37.119.202"}},"createdBy":"56b22dba38ff4c5042da1c77","createdTs":"2016-02-03T16:43:36.829Z","status":"ACTIVE","name":"Videostreamer","onClick":{"state":"^.devices.device","params":{"deviceId":"56b22e3838ff4c5042da1cb9"}}}};
    })


})();
