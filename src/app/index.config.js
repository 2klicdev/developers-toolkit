(function() {
  'use strict';

  angular
    .module('developersToolkit')
    .config(config);

  /** @ngInject */
  function config($logProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

  }

})();
